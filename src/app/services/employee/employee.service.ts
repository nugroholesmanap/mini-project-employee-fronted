// src/app/services/employee.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  private readonly employeeDataUrl = 'assets/dummy/employee-sample.json';

  constructor(private http: HttpClient) {}

  getEmployeeList(): Observable<any> {
    return this.http.get<any>(this.employeeDataUrl);
  }
}
