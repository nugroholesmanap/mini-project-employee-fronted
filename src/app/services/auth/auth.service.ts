import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated: boolean = false;
  private authToken: string = '';

  constructor() { }

  login(username: string, password: string): boolean {
    // Lakukan logika autentikasi, misalnya, memeriksa ke server backend
    // Jika berhasil, simpan informasi autentikasi
    this.isAuthenticated = true;
    this.authToken = 'example_token'; // Gantilah dengan token yang sebenarnya

    return this.isAuthenticated;
  }

  logout(): void {
    // Hapus informasi autentikasi saat logout
    this.isAuthenticated = false;
    this.authToken = '';
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  getAuthToken(): string {
    return this.authToken;
  }
}
