import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string = '';
  password: any = '';
  loginError: boolean = false;

  constructor(
    private router: Router,
    private titleService: Title
    ) { }

  ngOnInit(): void {
    this.titleService.setTitle('E-Employee [Login]');
  }
  
  login() {
    // Hard-coded login data for demonstration purposes
    const validUsername = 'demo';
    const validPassword = 'demo123';

    if (this.username === validUsername && this.password === validPassword) {
      this.router.navigate(['/employee-list']);
      this.loginError = false;
    } else {
      console.log('Invalid username or password');
      this.loginError = true;
    }
  }

}
