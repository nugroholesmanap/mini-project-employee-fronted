import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Employee } from '../../interfaces/employee';
import { formatCurrency } from '../../utils/format-currency';
import { EmployeeService } from '../../services/employee/employee.service';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {
  @ViewChild('modalDetailEmployee') modalDetailEmployee!: ElementRef;

  employees: Employee[] = []; 
  pageSize: number = 10;
  currentPage: number = 1;
  searchUsername: string = '';
  searchName: string = '';
  sortBy: string = 'name';
  sortDirection: string = 'asc';

  selectedEmployee: any; 

  constructor(
    private employeeService: EmployeeService,
    private titleService: Title
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle('E-Employee [Employee List Data]');
    this.fetchEmployeeData();
    if (this.modalDetailEmployee) {
      this.modalDetailEmployee.nativeElement.style.display = 'none';
    }
  }

  fetchEmployeeData(): void {
    this.employeeService.getEmployeeList().subscribe(
      (data) => {
        this.employees = data;
        console.log('employee', this.employees);
      },
      (error) => {
        console.error('Failed to fetch Employee data:', error);
      }
    );
  }

  showNotification(action: string): void {
    alert(`Aksi ${action} berhasil dilakukan!`);
  }

  // Implementasi fungsi paging
  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage--;
    }
  }

  nextPage(): void {
    if (this.currentPage < this.totalPages()) {
      this.currentPage++;
    }
  }

  totalPages(): number {
    return Math.ceil(this.employees.length / this.pageSize);
  }

  toggleSortDirection(): void {
    this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    this.sortData();
  }

  sortColumn(column: string): void {
    if (this.sortBy === column) {
      this.toggleSortDirection();
    } else {
      this.sortBy = column;
      this.sortDirection = 'asc';
      this.sortData();
    }
  }

  sortData(): void {
    this.employees.sort((a: any, b: any) => {
      const direction = this.sortDirection === 'asc' ? 1 : -1;
      if (a[this.sortBy] < b[this.sortBy]) {
        return -1 * direction;
      } else if (a[this.sortBy] > b[this.sortBy]) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  // Implementasi fungsi searching
  filterData(): Employee[] {
    let filteredData = [...this.employees];

    if (this.searchName) {
      filteredData = filteredData.filter(
        (employee) =>
          employee.firstName.toLowerCase().includes(this.searchName.toLowerCase()) ||
          employee.lastName.toLowerCase().includes(this.searchName.toLowerCase())
      );
    }
    if (this.searchUsername) {
      filteredData = filteredData.filter(
        (employee) =>
          employee.username.toLowerCase().includes(this.searchUsername.toLowerCase())
      );
    }

    return filteredData;
  }

  onSearch(){
    this.employees = this.filterData();
  }

  formatCurrency(basicSalary: number): string {
    return formatCurrency(basicSalary);
  }

  openModal(username: string): void {
    this.selectedEmployee = this.employees.find(employee => employee.username === username);
    if (this.modalDetailEmployee) {
      this.modalDetailEmployee.nativeElement.style.display = 'block';
      console.log('modal after', this.modalDetailEmployee);
    }
  }

  closeModal(): void {
    if (this.modalDetailEmployee) {
      this.modalDetailEmployee.nativeElement.style.display = 'none';
    }
  }

}
