// src/app/components/employee-add/employee-add.component.ts
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee-add.component.scss']
})
export class EmployeeAddComponent {

  employee = {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    basicSalary: 0,
    status: '',
    group: '',
    birthDate: '',
    description: ''
  };

  currentDate: any;
  dateToday: any;
  maxDate: any;
  minDate: any;

  constructor(
    private router: Router,
    private titleService: Title
  ) {
    this.getDateToday()
    console.log('today', this.dateToday);
  }

  ngOnInit() {
    this.titleService.setTitle('E-Employee [Employee Form Add]');
  }

  onSubmit(employeeForm: NgForm) {
    // Validasi semua input wajib diisi
    if (employeeForm.invalid) {
      alert('Please fill in all required fields.');
      return;
    }

    // Validasi format email
    const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (!emailPattern.test(this.employee.email)) {
      alert('Please enter a valid email address.');
      return;
    }

    // Validasi basic salary harus angka
    if (isNaN(this.employee.basicSalary)) {
      alert('Basic Salary must be a number.');
      return;
    }

    alert('Data saved successfully!');
  }

  onCancel() {
    this.router.navigate(['employee-list']);
  }

  getDateToday(){
    const today = new Date();

    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();

    const formattedDay = day < 10 ? `0${day}` : day;
    const formattedMonth = month < 10 ? `0${month}` : month;

    this.dateToday = year+'-'+formattedMonth+'-'+formattedDay;
  }
}
